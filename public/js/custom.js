$(function () {
  var a = $('footer ul li');
  a.css('width', (100 / a.length) + '%');

  $('.be-drop-down').on('click', function () {
    $(this).toggleClass('be-dropdown-active');
    $(this).find('.drop-down-list').stop().slideToggle();
  });
});

function comment_delete(loc) {
  $('.alert .close').on('click', function () {
    var parent = loc.substring(loc.lastIndexOf('/') + 1, loc.length);
    if ($(this).attr('data-type') == 'article') ajaxCall('/article/comment-del', 'article=' + parent + '&user='+$(this).attr('data-usr') +'&comment='+$(this).attr('data-src'));
    else ajaxCall('/message/delete', 'target=' + $(this).attr('data-src') + '&parent=' + parent);
  });
}

function child_form() {
  $(document).on('mouseleave', '.be-drop-down.be-dropdown-active', function(){
    $(this).click();
  });

  var checked = false;
  var form = $('#write');
  form.submit(function (e) {
    var code = $('input[name=code]').val();
    var org_code = (is_edit ? child.code : code);
    if (code && code != org_code) {
      $.ajax({
        type: 'POST',
        url: '/child/check',
        data: 'code=' + code,
        datatype: 'json',
        success: function (result) {
          if (!result.success) {
            alert(result.mes);
          } else {
            checked = true;
            form.unbind().submit();
          }
        }
      });
      if (!checked) e.preventDefault();
    }
  });
}

function child_supporters() {
  popup = $('#tag-dropdown');
  setting(popup, '/user');
}

function get_day_max(year, month) {
  var i = 29, cday;
  while (i < 32) {
    cday = new Date(year, month, i);
    if (cday.getFullYear() != year || cday.getMonth() != month) break;
    i++;
  }
  return i - 1;
}

var $year = $('#birth-year'), $month = $('#birth-month'), $day = $('#birth-day');

function birth_init(dateStr) {
  var i, year = (new Date()).getFullYear();
  for (i = year; i >= year - 60; i--) {
    $year.append("<option value='" + i + "'>" + i + "년</option>");
  }
  for (i = 1; i <= 12; i++) {
    $month.append("<option value='" + i + "'>" + i + "월</option>");
  }

  var $form = $('.birth-form');
  $form.find('select').find('option').each(function () {
    var val = $(this).val(), text = $(this).text();
    $(this).parent().parent().find('.drop-down-list').append('<li><a title="' + val + '">' + text + '</a></li>');
  });
  $form.find('.drop-down-list li').on('click', function () {
    $(this).parent().parent().find('.be-dropdown-content').text($(this).find('a').text());
    $(this).parent().parent().find('select').val($(this).find('a').attr('title'));
    birth_day();
  });

  if (dateStr != '') {
    var date = new Date(dateStr);
    $year.parent().find('.be-dropdown-content').text(date.getFullYear() + '년');
    $year.parent().find('select').val(date.getFullYear());
    $month.parent().find('.be-dropdown-content').text(date.getMonth() + 1 + '월');
    $month.parent().find('select').val(date.getMonth() + 1);
    $day.parent().find('.be-dropdown-content').text(date.getDate() + '일');
    $day.parent().find('select').val(date.getDate());
    birth_day();
  }
}

function birth_day() {
  var year = $year.val();
  var month = $month.val();

  if (!(year > 0 && month > 0)) return;
  var max = get_day_max(year, month - 1);
  $day.empty();
  $day.parent().find('.drop-down-list').empty();
  for (var i = 1; i <= max; i++) {
    $day.append("<option value='" + i + "'>" + i + "일</option>");
    $day.parent().find('.drop-down-list').append('<li><a title="' + i + '">' + i + '일</a></li>');
  }
  $day.parent().find('.drop-down-list li').on('click', function () {
    $(this).parent().parent().find('.be-dropdown-content').text($(this).find('a').text());
    $(this).parent().parent().find('select').val($(this).find('a').attr('title'));
  });
}