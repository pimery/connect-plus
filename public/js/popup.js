var popup = $('.message-popup');

$(function () {
  $('.open-button').on('click', function () {
    try {
      before_open($(this));
    } catch (err) {
      alert(err);
    }
    popup.slideDown();
  });
  $('.close-button').on('click', function () {
    popup.slideUp();
  });

  $('#comment-form').find('.form-input').keyup(function (e) {
    if (e.keyCode == 13) {
      return false;
    }
  });

  $('#comment-submit').on('click', function () {
    comment_submit();
  });

  $('#comment-form').submit(function () {
    comment_submit();
  });

  $('#open-file a.btn').on('click', function () {
    $(this).parent().find('input[type=file]').trigger('click');
  });
  
  $('#open-file input[type=file]').on('change', function () {
    if ($(this)[0].value.length <= 0) return;
    if (isImg($(this)[0].value)) {
      if (typeof (FileReader) != 'undefined') {
        $('.loader').removeAttr('style');
        var reader = new FileReader();
        reader.onload = function (e) {
          $('#open-file a').find('img').attr('src', e.target.result);
          $('.loader').hide();
        };
        reader.readAsDataURL($(this)[0].files[0]);
      } else {
        alert("선택된 파일 미리보기가 지원되지 않는 브라우저입니다");
      }
    } else {
      $(this).val('');
      alert("이미지만 선택해주세요");
    }
  });

  $('#video-del i').on('click', function () {
    $(this).parent().parent().find('input[name=rmVideo]').val(true);
    $(this).parent().fadeOut();
  });

  file_input($('#file-input'));
  initDropdownKeywords($('#tag-dropdown'));
});

function isImg(path) {
  var extn = path.substring(path.lastIndexOf('.') + 1).toLowerCase();
  return (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg");
}

function file_input($container) {
  $container.find('.pre i').on('click', function () {
    $container.find('select option').eq($(this).parent().attr('data')).attr('selected', true);
    $(this).parent().remove();
  });
  $container.find('.list_keywords a').eq(0).on('click', function () {
    $container.find('select option').prop('selected', true);
    $container.find('input[type=file]').val('');
    $(this).parent().find('.color-6').remove();
  });
  $container.find('input[type=file]').on('change', function () {
    var list = $container.find('.list_keywords');
    list.find('.uploaded').remove();
    if ($(this)[0].value.length <= 0) return;
    if (isImg($(this)[0].value)) {
      if (typeof (FileReader) != "undefined") {
        var len = $(this)[0].files.length;
        $('.loader').removeAttr('style');
        for (var i = 0; i < len; i++) {
          var reader = new FileReader();
          reader.onload = function (e) {
            list.append('<a class="btn color-6 size-3 hover-10 uploaded"><img src="' + e.target.result + '"/><i class="fa fa-times keyword"></i></a>');
            if (list.find('.uploaded').length == len) $('.loader').hide();
          };
          reader.readAsDataURL($(this)[0].files[i]);
        }
      } else {
        alert("선택된 파일 미리보기가 지원되지 않는 브라우저입니다");
      }
    } else {
      $(this).val('');
      alert("이미지만 선택해주세요");
    }
  });
}

function comment_submit() {
  var form = $('#comment-form');
  var cont = form.find('input[name=content]').val();
  if (cont.length > 0) {
    var url = '/reply/' + $('#comment-submit').attr('title');
    var data = 'parent=' + form.find('input[name=parent]').val() + '&content=' + cont;
    ajaxCall(url, data);
  }
}

function setting($btn, link) {
  $.ajax({
    url: link + '/list/' + $btn.attr('title'),
    success: function (result) {
      popup.find('select').find('option').remove();
      if (result.err) {
        popup.find('select').append('<option value="">' + result.err + '</option>');
      } else {
        result.list.forEach(function (item) {
          if (item.selected) {
            popup.find('select').append('<option value="' + item._id + '" selected>' + item.name + '</option>');
          } else {
            popup.find('select').append('<option value="' + item._id + '">' + item.name + '</option>');
          }
        });
        initDropdownKeywords(popup);
      }
    }
  });
  return $btn.attr('title');
}

function user_setting() {
  $('.supporter').on('click', function () {
    ajaxCall('/user/supporter', 'user=' + $(this).attr('title'));
  });
  $('.supporter-del').on('click', function () {
    ajaxCall('/user/supporter-del', 'user=' + $(this).attr('title'));
  });
  $('#submit-button').on('click', function () {
    if (targ_id === '') return;
    ajaxCall('/setting/child', 'target=' + targ_id + '&list=' + popup.find('select').val());
  });
}

function child_setting() {
  $('#submit-button').on('click', function () {
    if (targ_id === '') return;
    ajaxCall('/setting/user', 'target=' + targ_id + '&list=' + popup.find('select').val());
  });
}

function initDropdownKeywords($container) {
  $container.find('.drop-down-list li').remove();
  $container.find('.list_keywords .color-6').remove();
  $container.find('select').find('option').each(function () {
    var val = $(this).val(), text = $(this).text();
    $container.find('.drop-down-list').append('<li><a title="' + val + '">' + text + '</a></li>');
    $container.find('.list_keywords').append('<a title="' + val + '" class="btn color-6 size-3 hover-10" style="display:none">' + text + '<i class="fa fa-times keyword"></i></a>');
    if ($(this).attr('selected')) {
      listSel($container, val);
    }
  });
  $container.find('.drop-down-list li').on('click', function () {
    $(this).parent().parent().find('.be-dropdown-content').text($(this).find('a').text());
    listSel($container, $(this).find('a').attr('title'), $(this).find('a').text());
  });
  $container.find('.list_keywords i').on('click', function () {
    listDel($container, $(this).parent().attr('title'));
  });
  $container.find('.list_keywords a').eq(0).on('click', function () {
    $(this).parent().parent().find('.be-dropdown-content').text($(this).text());
    $(this).parent().find('.color-6').each(function () {
      listDel($container, $(this).attr('title'));
    });
  });
}

function listSel($container, val) {
  $container.find('select').find('option[value="' + val + '"]').attr('selected', true);
  $container.find('.list_keywords').find('a[title="' + val + '"]').fadeIn();
  $container.find('.drop-down-list').find('a[title="' + val + '"]').parent().fadeOut();
}

function listDel($container, val) {
  $container.find('select').find('option[value="' + val + '"]').removeAttr('selected');
  $container.find('.list_keywords').find('a[title="' + val + '"]').fadeOut();
  $container.find('.drop-down-list li').find('a[title="' + val + '"]').parent().fadeIn();
}

function before_delete(link, id, text, redirect) {
  popup = $('.delete-popup');
  popup.find('.ob-delete').on('click', function () {
    ajaxCall(link + 'delete', 'target=' + JSON.parse(id), redirect);
  });
  if (text) popup.find('.form-input').val(text);
}

function ajaxCall(url, data, redirect) {
  $.ajax({
    type: 'POST',
    url: url,
    data: data,
    datatype: 'json',
    success: function (result) {
      if (result.mes) alert(result.mes);
      if (result.success) {
        if (redirect) location.href = redirect;
        else location.reload();
      }
      else popup.slideUp();
    }
  })
}