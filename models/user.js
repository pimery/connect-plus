var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var relationship = require("mongoose-relationship");
var common = require('../config/common');
var Schema = mongoose.Schema;

var userSchema = new Schema({
  username: {type: String, required: true, unique: true},
  local: {
    email: String,
    password: String,
    firstName: String,
    lastName: String,
    gender: Boolean,
    birth: Date,
    photo: String,
    children: [{type: Schema.ObjectId, ref: 'Child', childPath: 'supports'}]
  },
  facebook: {
    email: String,
    name: String,
    photo: String
  },
  rank: {type: Number, default: 1},
  likes: [{type: Schema.ObjectId, ref: 'Article'}],
  shares: [{type: Schema.ObjectId, ref: 'Article'}],
  messages: [{type: Schema.ObjectId, ref: 'Message'}]
});

userSchema.methods.isAdmin = function () {
  return (this.rank < 0);
};

userSchema.methods.isSupporter = function () {
  return (this.rank > 1);
};

userSchema.methods.isUnChecked = function () {
  return (this.rank === 1);
};

userSchema.methods.isVisitor = function () {
  return (this.rank >= 10);
};

userSchema.methods.generateHash = function (password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.local.password);
};

userSchema.methods.getName = function () {
  if (this.facebook.email == null)
    return this.local.lastName + this.local.firstName;
  return this.facebook.name;
};

userSchema.methods.getMail = function () {
  if (this.facebook.email)
    return this.facebook.email;
  if (this.local.email)
    return this.local.email;
  return common.N_INFO;
};

userSchema.methods.getPhoto = function () {
  if (this.facebook.email != null && this.facebook.photo != null) {
    return this.facebook.photo;
  }
  if (this.local.photo != null) {
    return common.USER_PATH + this.username + '.' + this.local.photo;
  }
  return common.DEF_USER;
};

userSchema.plugin(relationship, {relationshipPathName: 'local.children'});
module.exports = mongoose.model('User', userSchema);