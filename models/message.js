var mongoose = require('mongoose');
var relationship = require('mongoose-relationship');
var common = require('../config/common');
var time = require('../config/time');
var Schema = mongoose.Schema;

var message = new Schema({
  user: {type: Schema.ObjectId, ref: 'User', childPath: 'messages'},
  depth: {type: Number, default: 0},
  parent: {type: Schema.ObjectId, ref: 'Message'},
  children: [{type: Schema.ObjectId, ref: 'Message'}],
  photos: [{type: String}],
  content: String,
  dateTime: {type: Date, default: Date.now},
  editTime: {type: Date, default: Date.now},
  public: {type: Boolean, default: false},
  like: {type: Number, default: 0}
});

message.methods.getCover = function () {
  if (this.photos.length > 0) {
    return common.MES_PATH + this.photos[0];
  }
};

message.methods.getPhoto = function (index) {
  if (this.photos[index] == null) return null;
  return common.MES_PATH + this.photos[index];
};

message.plugin(relationship, {relationshipPathName: 'user'});
module.exports = mongoose.model('Message', message);