var mongoose = require('mongoose');
var relationship = require('mongoose-relationship');
var common = require('../config/common');
var Schema = mongoose.Schema;

var childSchema = new Schema({
  supports: [{type: Schema.ObjectId, ref: 'User', childPath: 'local.children'}],
  articles: [{type: Schema.ObjectId, ref: 'Article', childPath: 'children'}],
  name: {type: String, required: true, unique: true},
  age: Number,
  isMale: Boolean,
  birth: Date,
  school: String,
  code: {type: String, required: true, unique: true},
  favoriteSubject: String,
  favoriteActivity: String,
  photo: String
});

childSchema.methods.getPhoto = function () {
  if (this.photo != null) return common.CHILD_PATH + this._id + '.' + this.photo;
  return common.DEF_USER;
};

childSchema.plugin(relationship, {relationshipPathName: 'supports'});
childSchema.plugin(relationship, {relationshipPathName: 'articles'});
module.exports = mongoose.model('Child', childSchema);