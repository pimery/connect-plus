var mongoose = require('mongoose');
var relationship = require("mongoose-relationship");
var common = require('../config/common');
var time = require('../config/time');
var Schema = mongoose.Schema;

var articleSchema = new Schema({
  comments: [{
    id: {type: Schema.ObjectId},
    user: {type: Schema.ObjectId, ref: 'User'},
    content: String,
    dateTime: {type: Date, default: Date.now},
    editTime: {type: Date, default: Date.now},
    like: {type: Number, default: 0}
  }],
  photos: [{type: String}],
  video: String,
  title: String,
  content: String,
  dateTime: {type: Date, default: Date.now},
  editTime: {type: Date, default: Date.now},
  like: {type: Number, default: 0},
  share: {type: Number, default: 0},
  children: [{type: Schema.ObjectId, ref: 'Child', childPath: 'articles'}]
});

articleSchema.methods.getCover = function () {
  if (this.photos.length > 0) {
    return common.ART_PATH + this.photos[0];
  }
};

articleSchema.methods.getVideo = function () {
  if (this.video) {
    return common.VIDEO_PATH + this.video;
  }
};

articleSchema.plugin(relationship, {relationshipPathName: 'children'});
module.exports = mongoose.model('Article', articleSchema);