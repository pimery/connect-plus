var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var common = require('../config/common');
var User = require('../models/user');
var auth = require('./auth');
var passport = require('passport');

module.exports = function (app) {
  app.use(passport.initialize());
  app.use(passport.session());

  passport.serializeUser(function (user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
      done(err, user);
    });
  });

  passport.use('local-login', new LocalStrategy(
    {passReqToCallback: true},
    function (req, username, password, done) {
      User.findOne({'username': username}, function (err, user) {
        if (err) return done(err);
        if (!user) {
          req.flash('error', common.ERR_ID);
          return done(null, false);
        }
        if (!user.validPassword(password)) {
          req.flash('error', common.ERR_PASS);
          return done(null, false);
        }
        return done(null, user);
      });
    }
  ));

  passport.use(new FacebookStrategy(
    {
      clientID: auth.facebookAuth.clientID,
      clientSecret: auth.facebookAuth.clientSecret,
      callbackURL: auth.facebookAuth.callbackURL,
      scope: "email",
      profileFields : ["id", "name", "link", "gender", "locale", "displayName", "photos", "email"]
    },
    function (token, refreshToken, profile, done) {
      process.nextTick(function () {
        User.findOne({'username': profile.id}, function (err, user) {
          if (err) return done(err);
          var email = profile.emails[0].value,
            name = profile.displayName,
            photo = profile.photos[0].value;
          if (user) {
            if (email && user.facebook.email != email)
              user.facebook.email = email;
            if (name && user.facebook.name != name)
              user.facebook.name = name;
            if (photo && user.facebook.photo != photo)
              user.facebook.photo = photo;
            user.save();
            return done(null, user);
          } else {
            var newUser = new User({username: profile.id});
            newUser.facebook.email = email;
            newUser.facebook.name = name;
            newUser.facebook.photo = photo;
            newUser.rank = 10;
            newUser.save();
            return done(null, newUser);
          }
        });
      });
    }
  ));
  return passport;
};