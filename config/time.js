String.prototype.string = function (len) {
  var s = '', i = 0;
  while (i++ < len) {
    s += this;
  }
  return s;
};
String.prototype.zf = function (len) {
  return "0".string(len - this.length) + this;
};
Number.prototype.zf = function (len) {
  return this.toString().zf(len);
};
Date.prototype.format = function (f) {
  if (!this.valueOf()) return " ";

  var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
  var d = this;

  return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function ($1) {
    switch ($1) {
      case "yyyy":
        return d.getFullYear();
      case "yy":
        return (d.getFullYear() % 1000).zf(2);
      case "MM":
        return (d.getMonth() + 1).zf(2);
      case "dd":
        return d.getDate().zf(2);
      case "E":
        return weekName[d.getDay()];
      case "HH":
        return d.getHours().zf(2);
      case "hh":
        return ((h = d.getHours() % 12) ? h : 12).zf(2);
      case "mm":
        return d.getMinutes().zf(2);
      case "ss":
        return d.getSeconds().zf(2);
      case "a/p":
        return d.getHours() < 12 ? "오전" : "오후";
      default:
        return $1;
    }
  });
};

exports.getTime = function (val) {
  var now = new Date();
  var value = new Date(val);
  var date, time;

  if (now.getFullYear() != value.getFullYear()) {
    date = value.format("'yy/MM/dd");
    time = value.format("HH:mm");
  } else {
    var dateGap = now.getTime() - value.getTime();
    var timeGap = new Date(0, 0, 0, 0, 0, 0, now - value);
    var diffHour = timeGap.getHours();
    var diffMin = timeGap.getMinutes();
    var diffSec = timeGap.getSeconds();

    if (Math.floor(dateGap / (1000 * 60 * 60 * 24)) > 0) {
      date = value.format("MM/dd");
      time = value.format("HH:mm");
    } else {
      date = "";
      if (diffHour > 0) {
        time =  diffHour + "시간 전";
      } else if (diffMin > 0) {
        time =  diffMin + "분 전";
      } else {
        time =  "방금 전";
      }
    }
  }

  return {date:date, time:time};
};