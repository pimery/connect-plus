var Child = require('../models/child.js');
var User = require('../models/user.js');
var time = require('./time');
var async = require('async');
var fs = require('fs');

var _this = this, calls = [];

exports.detail = function (req, res, model, listName, is_article) {
  _this.isSupportCheck(req);
  calls = [];
  var list = [];
  model.findById(req.params.id, function (err, ob) {
    if (err) return _this.flash(req, res, 'detail ' + err);
    if (!ob) return _this.flash(req, res, _this.ERR_NONE);
    _this.setTimeInfo(ob);
    if (is_article) _this.getReact(ob, req.user, true);
    else {
      calls.push(function (callback) {
        User.findById(ob.user, function (err, user) {
          ob.userOb = _this.getUser(err, user);
          _this.getReact(ob, user, false);
          callback(null);
        })
      });
    }
    if (ob[listName].length > 0) {
      if (is_article) {
        ob[listName].forEach(function (reply) {
          _this.setTimeInfo(reply);
          calls.push(function (callback) {
            User.findById(reply.user, function (err, user) {
              reply.userOb = _this.getUser(err, user);
              if (req.user.isAdmin() || req.user._id.equals(reply.userOb._id)) reply.owner = true;
              list.push(reply);
              callback(null);
            });
          });
        })
      } else {
        calls.push(function (callback) {
          model.find({_id: {$in: ob[listName]}}, function (err, replies) {
            if (err) callback('detail replies ' + err);
            if (!replies) callback(null);
            var subs = [];
            replies.forEach(function (reply) {
              _this.setTimeInfo(reply);
              subs.push(function (callback2) {
                User.findById(reply.user, function (err, user) {
                  reply.userOb = _this.getUser(err, user);
                  if (req.user.isAdmin() || req.user._id.equals(reply.userOb._id)) reply.owner = true;
                  list.push(reply);
                  callback2(null);
                });
              });
            });
            async.series(subs, function (err) {
              callback(err);
            });
          });
        });
      }
    }
    if (is_article && ob.children.length > 0) {
      calls.push(function (callback) {
        Child.find({_id: {$in: ob.children}}, function (err, children) {
          if (err) callback('detail children ' + err);
          if (!children) callback(null);
          ob.tags = [];
          if (Array.isArray(children)) children.forEach(function (child) {
            ob.tags.push(child.name);
          });
          callback(null);
        });
      });
    }
    async.series(calls, function (err) {
      if (err) return _this.flash(req, res, err);
      list.sort(function (a, b) {
        return a.dateTime - b.dateTime;
      });
      return _this.render(req, res, 'details', {
        title: (is_article) ? ob.title : '소통공간',
        menu_home: is_article,
        menu_chat: !is_article,
        object: ob,
        len: ob[listName].length,
        list: list,
        link: (is_article) ? 'article' : 'message'
      });
    });
  });
};

exports.error = function (err, req, res, next) {
  if (err.code) {
    if (err.code === 'LIMIT_FILE_SIZE') req.flash('error', '10MB 이하의 파일만 올릴 수 있습니다');
    return res.redirect(req.get('referer'));
  }
  res.status(err.status || 500);
  console.log('err ' + JSON.stringify(err));
  return _this.flash(req, res, err.message);
};

exports.flash = function (req, res, mes) {
  return _this.render(req, res, 'flash', {title: '알림', mes: mes});
};

exports.getFileExt = function (fileName) {
  var fileExt = fileName.split(".");
  if (fileExt.length === 1 || ( fileExt[0] === "" && fileExt.length === 2 )) {
    return "";
  }
  return fileExt.pop();
};

exports.getReact = function (object, user, is_article) {
  if (!user) return;
  if (_this.isInList(user.likes, object._id)) {
    object.liked = true;
  }
  if (is_article && _this.isInList(user.shares, object._id)) {
    object.shared = true;
  }
};

exports.getUser = function (err, user) {
  if (err || !user) {
    if (err) console.log('get user ' + err);
    user = {'setName': _this.ERR_USER, 'setPhoto': _this.DEF_USER};
    return user;
  } else {
    user.setName = user.getName();
    user.setPhoto = user.getPhoto();
  }
  return user;
};

exports.isAdminCheck = function (req) {
  if (!req.user.isAdmin()) throw new Error(_this.ERR_ADMIN);
};

exports.isInList = function (list, id) {
  var result = list.indexOf(id);
  if (result < 0) {
    if (Array.isArray(list))
      list.forEach(function (item, i) {
        if (item.equals(id)) {
          result = i;
        }
      });
  }
  return result >= 0;
};

exports.isNull = function (ob) {
  return (ob == undefined);
};

exports.isSupportCheck = function (req) {
  if (req.user.isVisitor() || !(req.user.isSupporter() || req.user.isAdmin())) throw new Error(_this.ERR_SUP);
};

exports.isWriterCheck = function (req, user) {
  if (req.user.isAdmin()) return;
  if (!req.user._id.equals(user)) throw new Error(_this.ERR_WRT);
};

exports.list = function (req, res, model, model2, sort, is_user) {
  !_this.isAdminCheck(req);
  var list = [];
  calls = [];
  model.findById(req.params.id, function (err, ob) {
    if (err) return res.send({err: 'list ' + err});
    if (!ob) return res.send({err: _this.ERR_NO});
    calls.push(function (callback) {
      model2.find({}, null, {sort: sort}, function (err, all) {
        if (err) callback('list all ' + err);
        if (!all) callback(common.ERR_LIST);
        all.forEach(function (item) {
          if (!is_user && (item.isVisitor() || !(item.isAdmin() || item.isSupporter()))) return;
          list.push({
            _id: item._id,
            name: (is_user) ? item.name + "(" + item.code + ")" : item.getName(),
            selected: _this.isInList((is_user) ? ob.local.children : ob.supports, item._id)
          });
        });
        callback(null);
      });
    });
    async.series(calls, function (err) {
      return res.send({err: err, list: list});
    });
  });
};

exports.push = function (list, id) {
  if (!_this.isInList(list, id)) list.push(id);
  return list;
};

exports.react = function (req, res, model, type, redirect) {
  var id = req.params.id;
  model.findById(id, function (err, object) {
    if (err) return _this.flash(req, res, 'object ' + type + ' err ' + err);
    if (!object) return _this.flash(req, res, _this.ERR_NONE);
    if (object) {
      user = req.user;
      if (!_this.isInList(user[type + 's'], object._id)) {
        user[type + 's'].push(object);
        object[type] = object[type] + 1;
      } else {
        user[type + 's'].splice(user[type + 's'].indexOf(object), 1);
        if (object[type] > 0) object[type] = object[type] - 1;
      }
      user.save();
      object.save();
      return res.redirect(redirect + id);
    }
  });
};

exports.reduceList = function (list) {
  list.reduce(function (a, b) {
    if (a.indexOf(b) < 0)a.push(b);
    return a;
  }, []);
  return list;
};

exports.render = function (req, res, page, data) {
  if (req.isAuthenticated() && data) {
    data.isLogin = req.isAuthenticated();
    data.isAdmin = req.user.isAdmin();
    data.isSupport = data.isAdmin || req.user.isSupporter();
  }
  return res.render(page, data);
};

exports.reply = function (req, res, model, is_article) {
  _this.isSupportCheck(req);
  calls = [];
  model.findById(req.body.parent, function (err, ob) {
    if (err) return res.send({mes: 'reply ' + err});
    if (!ob) return _this.flash(req, res, _this.ERR_NONE);
    var reply = {user: req.user._id, content: req.body.content};
    if (!is_article) {
      var child = new model(reply);
      child.depth = ob.depth + 1;
      child.parent = ob._id;
      child.save();
      ob.children = _this.push(ob.children, child);
    }
    else ob.comments = _this.push(ob.comments, reply);
    ob.save();
    return res.send({success: true});
  });
};

exports.setTimeInfo = function (object) {
  var datetime;
  if (new Date(object.dateTime).valueOf() !== new Date(object.editTime).valueOf()) {
    datetime = time.getTime(object.editTime);
    object['edit'] = datetime.date + ' ' + datetime.time;
  }
  datetime = time.getTime(object.dateTime);
  object['date'] = datetime.date;
  object['time'] = datetime.time;
};

exports.sortName = function (a, b) {
  if (a.name < b.name)
    return -1;
  else if (a.name > b.name)
    return 1;
  else
    return 0;
};

exports.support = function (req, res, model, is_user) {
  _this.isAdminCheck(req);
  var a = req.body.target, list = req.body.list.split(',');
  if (typeof a === 'undefined' && typeof list === 'undefined')
    return res.send({mes: _this.N_INFO});
  model.findById(a, function (err, ob) {
    if (err) return res.send({mes: 'support ' + err});
    if (!ob) return res.send({mes: '잘못된 대상입니다'});
    if (is_user) ob.local.children = [];
    else ob.supports = [];
    list.forEach(function (item) {
      if (!item || item === 'null') return;
      if (is_user) _this.push(ob.local.children, item);
      else _this.push(ob.supports, item);
    });
    ob.save();
    return res.send({success: true, mes: '추가되었습니다'});
  });
};

exports.unlink = function (path) {
  try {
    fs.unlinkSync(__dirname + '/../public' + path);
  } catch (err) {
    console.log('[unlink err]' + err);
  }
};

//PATH
exports.ART_PATH = '/upload/article/';
exports.CHILD_PATH = '/upload/children/';
exports.MES_PATH = '/upload/message/';
exports.USER_PATH = '/upload/user/';
exports.VIDEO_PATH = '/upload/video/';
//IMG
exports.DEF_USER = '/nrg/img/ava_10.jpg';
//ERR
exports.ERR = '오류가 발생하였습니다';
exports.ERR_ADMIN = '관리자 권한이 없습니다';
exports.ERR_CHILD = '해당 아동이 없습니다';
exports.ERR_ID = '등록되지 않은 아이디입니다';
exports.ERR_LIST = '목록이 존재하지 않습니다';
exports.ERR_NO = '대상이 존재하지 않습니다';
exports.ERR_NONE = '게시물이 존재하지 않습니다';
exports.ERR_PASS = '비밀번호를 확인해주세요';
exports.ERR_PASS_PRE = '기존 비밀번호를 확인해주세요';
exports.ERR_PASS_CHECK = '새 비밀번호를 확인해주세요';
exports.ERR_SUP = '후원자만 이용 가능합니다';
exports.ERR_USEDID = '등록되지 않은 아이디입니다';
exports.ERR_USER = '존재하지 않는 사용자';
exports.ERR_WRT = '수정할 권한이 없습니다';
//NONE
exports.N_INFO = '입력된 정보가 없습니다';
exports.N_SHARE = '담아둔 게시글이 없습니다';
exports.N_CHILD = '후원하는 아동이 없습니다';
exports.N_CHILD_ART = '후원하는 아동의 소식이 없습니다';
exports.N_ART = '작성한 게시물이 없습니다';