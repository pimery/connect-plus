var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var flash = require('express-flash');
var common = require('./config/common');

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(session({secret: 'mySecretKey'}));
app.use(flash());
app.use(express.static(path.join(__dirname, 'public')));

require('./router.js').route(app);
require('./db.js').connect();

app.use(function (req, res, next) {
  var err = new Error('페이지를 찾을 수 없습니다');
  err.status = 404;
  next(err);
});

// error handlers
if (app.get('env') === 'development') {
  app.use(common.error);
}
app.use(common.error);

module.exports = app;
