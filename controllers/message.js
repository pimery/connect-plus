var User = require('../models/user.js');
var Message = require('../models/message.js');
var common = require('../config/common');
var async = require('async');
var multer = require('multer');

var calls = [];

function saveMessage(message, req) {
  message.content = req.body.content;
  if (Array.isArray(req.body.remove)) req.body.remove.forEach(function (item) {
    if (!item) return;
    common.unlink(common.MES_PATH + item);
    message.photos.splice(message.photos.indexOf(item), 1);
  }); else if (req.body.remove) {
    common.unlink(common.MES_PATH + req.body.remove);
    message.photos.splice(message.photos.indexOf(req.body.remove), 1);
  }
  if (req.body.public == 'on') message.public = true;
  if (typeof req.files !== 'undefined')
    for (var i = 0; i < req.files.length; i++)
      message.photos.push(req.files[i].filename);
  message.editTime = Date.now();
  return message;
}

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, __dirname + '/../public' + common.MES_PATH);
  },
  filename: function (req, file, cb) {
    cb(null, new Date().toString() + '_' + file.originalname);
  }
});

exports.upload = multer({storage: storage});

exports.all = function (req, res) {
  common.isSupportCheck(req);
  var list = [], calls = [];
  Message.find({depth: 0, public: true}, null, {sort: '-dateTime'}, function (err, messages) {
    if (err) return common.flash(req, res, 'message all err ' + err);
    if (Array.isArray(messages)) messages.forEach(function (item) {
      calls.push(function (callback) {
        User.findOne({_id: item.user}, function (err, user) {
          item.userOb = common.getUser(err, user);
          common.setTimeInfo(item);
          list.push(item);
          callback(null);
        });
      });
    });
    async.series(calls, function (err) {
      if (err) return common.flash(req, res, err);
      return common.render(req, res, 'chat', {title: '소통공간', menu_chat: true, messages: list});
    });
  });
};

exports.delete = function (req, res) {
  if (typeof req.body.target === 'undefined')
    return res.send({mes: common.N_INFO});
  Message.findById(req.body.target, function (err, message) {
    if (err) return res.send({mes: 'mes del ' + err});
    if (!message) return res.send({mes: common.ERR_NONE});
    common.isWriterCheck(req, message.user);
    if (typeof req.body.parent === 'undefined') {
      message.photos.forEach(function (photo) {
        common.unlink(common.MES_PATH + photo);
      });
      message.remove();
      return res.send({success: true});
    } else {
      calls = [];
      calls.push(function (callback) {
        Message.findById(req.body.parent, function (err, parent) {
          if (err) callback({mes: 'mes parent del ' + err});
          if (!parent) callback({mes: 'mes parent ' + common.ERR_NONE});
          var index = parent.children.indexOf(req.body.target);
          parent.children.splice(index, 1);
          parent.save();
          callback(null);
        });
      });
      async.series(calls, function (err) {
        if (err) return res.send(err);
        message.remove();
        return res.send({success: true});
      });
    }
  });
};

exports.detail = function (req, res) {
  return common.detail(req, res, Message, 'children', false);
};

exports.edit = function (req, res) {
  common.isSupportCheck(req);
  calls = [];
  Message.findById(req.params.id, function (err, message) {
    if (err) return common.flash(req, res, 'message edit err' + err);
    if (!message) return common.flash(req, res, common.ERR_NONE);
    common.isWriterCheck(req, message.user);
    if (req.method !== 'GET') {
      saveMessage(message, req).save();
      return res.redirect('/message/' + message._id);
    }
    return common.render(req, res, 'message', {title: '메세지 작성', menu_chat: true, message: message});
  });
};

exports.like = function (req, res) {
  common.isSupportCheck(req);
  common.react(req, res, Message, 'like', '/message/');
};

exports.list_user = function (req, res) {
  calls = [];
  var result = [];
  var user = req.body.user;
  var messages = user.messages;
  if (Array.isArray(messages)) messages.forEach(function (item) {
    calls.push(function (callback) {
      Message.findById(item, function (err, message) {
        common.setTimeInfo(message);
        article.userob = user;
        result.push(message);
        callback(null);
      });
    });
  });
  async.series(calls, function (err) {
    if (err)
      return console.log(err);
    res.end(JSON.stringify(result));
  });
};

exports.reply = function (req, res) {
  return common.reply(req, res, Message, false);
};

exports.write = function (req, res) {
  common.isSupportCheck(req);
  if (req.method === 'GET') {
    var fromMypage = common.isInList(req.get('referer'), 'mypage');
    return common.render(req, res, 'message', {title: '메세지 작성', menu_chat: !fromMypage, menu_mypage: fromMypage});
  }
  var message = new Message({user: req.user._id});
  saveMessage(message, req).save();
  async.filter([message._id], function (elem, callback) {
    Message.findOne({_id: elem}, function (err, doc) {
      callback(err == null && doc != null);
    });
  }, function () {
    return res.redirect('/message/' + message._id);
  });
};