var Article = require('../models/article.js');
var Message = require('../models/message.js');
var Child = require('../models/child.js');
var common = require('../config/common');
var async = require('async');
var fs = require('fs');
var multer = require('multer');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, __dirname + '/../public' + common.USER_PATH);
  },
  filename: function (req, file, cb) {
    cb(null, req.user.username + '.' + common.getFileExt(file.originalname));
  }
});

exports.upload = multer({storage: storage});

exports.detail = function (req, res) {
  common.isSupportCheck(req);
  var calls = [], list = [];
  list.articles = [];
  list.share = [];
  list.message = [];
  list.children = [];
  user = req.user;
  if (user.messages.length) {
    calls.push(function (callback) {
      Message.find({_id: {$in: user.messages}, depth: 0}, null, {sort: '-dateTime'}, function (err, messages) {
        if (err) callback('mypage message ' + err);
        if (Array.isArray(messages)) messages.forEach(function (message) {
          common.setTimeInfo(message);
          list.message.push(message);
        });
        callback(null);
      });
    });
  }
  if (user.shares.length) {
    calls.push(function (callback) {
      Article.find({_id: {$in: user.shares}}, null, {sort: '-dateTime'}, function (err, articles) {
        if (err) callback('mypage shares ' + err);
        if (Array.isArray(articles)) articles.forEach(function (article) {
          common.setTimeInfo(article);
          list.share.push(article);
        });
        callback(null);
      });
    });
  }
  calls.push(function (callback) {
    Child.find({_id: {$in: user.local.children}}, null, {sort: 'name'}, function (err, children) {
      if (err) callback('mypage children ' + err);
      if (Array.isArray(children)) children.forEach(function (child) {
        list.children.push({_id: child._id, name: child.name, photo: child.getPhoto()});
        child.articles.forEach(function (id) {
          if (!common.isInList(list.articles, id))
            list.articles.push(id);
        });
      });
      if (list.articles.length > 0) {
        var subs = [];
        subs.push(function (callback) {
          Article.find({_id: {$in: list.articles}}, null, {sort: '-dateTime'}, function (err, articles) {
            if (err) callback('mypage articles ' + err);
            list.articles = [];
            if (Array.isArray(articles)) articles.forEach(function (article, i) {
              common.setTimeInfo(article);
              list.articles.push(article);
            });
            callback(null);
          });
        });
        async.series(subs, function (err) {
          callback(err);
        })
      } else callback(null);
    });
  });
  async.series(calls, function (err) {
    if (err) return common.flash(req, res, err);
    return common.render(req, res, 'mypage', {
      title: '개인정보',
      menu_mypage: true,
      body: 'blog',
      user: user,
      articles: list.articles,
      children: list.children,
      shares: list.share,
      messages: list.message
    });
  });
};

exports.edit = function (req, res) {
  common.isSupportCheck(req);
  user = req.user;
  if (req.method == 'GET') {
    return common.render(req, res, 'mypage_edit', {
      title: user.getName(),
      menu_mypage: true,
      user: user,
      object: user
    });
  }
  if (!user.validPassword(req.body.pass1)) {
    req.flash('error', common.ERR_PASS);
    return res.redirect('/mypage/edit');
  }
  if ((req.body.password || req.body.pass2) && (req.body.password !== req.body.pass2)) {
    req.flash('error', common.ERR_PASS_CHECK);
    return res.redirect('/mypage/edit');
  }
  if (typeof req.file !== 'undefined') {
    if (user.local.photo != null && user.local.photo !== common.getFileExt(req.file.filename))
      common.unlink(user.getPhoto());
    user.local.photo = common.getFileExt(req.file.filename);
  }
  if (req.body.password) user.local.password = user.generateHash(req.body.password);
  user.local.firstName = req.body.firstName;
  user.local.lastName = req.body.lastName;
  user.local.birth = new Date(req.body.birthYear, req.body.birthMonth - 1, req.body.birthDay);
  user.save();
  return res.redirect('/mypage');
};