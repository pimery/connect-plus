var passport = require('passport');
var common = require('../config/common');
var Child = require('../models/child');
var User = require('../models/user');
var async = require('async');

var calls = [];

exports.auth = function (req, res, next) {
  req.session.returnTo = req.originalUrl;
  if (req.isAuthenticated())
    return next();
  req.flash('error', '로그인이 필요합니다');
  res.redirect('/login');
};

exports.list = function (req, res) {
  return common.list(req, res, Child, User, 'username', false);
};

exports.login = function (req, res, next) {
  if (req.method === 'GET') {
    if (req.isAuthenticated()) return res.redirect('/home');
    return res.render('login', {title: '로그인'});
  }
  passport.authenticate('local-login', function (err, user) {
    if (err) return next(err);
    if (!user) return res.redirect('/login');
    req.logIn(user, function (err) {
      if (err) return next(err);
      var returnTo = req.session.returnTo;
      delete req.session.returnTo;
      return res.redirect(returnTo || '/home');
    });
  })(req, res, next);
};

exports.loginFacebook = function (req, res, next) {
  passport.authenticate('facebook', function (err, user) {
    if (err) return next(err);
    if (!user) return res.redirect('/login');
    req.logIn(user, function (err) {
      if (err) return next(err);
      return res.redirect('/home');
    });
  })(req, res, next);
};

exports.logout = function (req, res) {
  delete req.session.returnTo;
  req.logOut();
  res.redirect('/login');
};

exports.newsfeed = function (req, res) {
  res.send('if you are viewing this page it means you are logged in');
};

exports.setting = function (req, res) {
  common.isAdminCheck(req);
  if (req.method !== 'GET') {
    return common.render(req, res, 'user_setting');
  }
  var result = [];
  calls = [];
  User.find({}, function (err, users) {
    if (err) return common.flash(req, res, 'user setting ' + err);
    var list, uncheck = [];
    if (Array.isArray(users)) users.forEach(function (user) {
      if (user.isVisitor()) return;
      if (user.isUnChecked()) {
        uncheck.push({_id: user._id, name: user.getName()});
        return;
      }
      calls.push(function (callback) {
        Child.find({_id: {$in: user.local.children}}, function (err, children) {
          if (err) callback(err);
          list = [];
          if (Array.isArray(children)) children.forEach(function (child) {
            list.push(child.name);
          });
          result.push({_id: user._id, name: user.getName(), children: list, admin: user.isAdmin()});
          callback(null);
        })
      });
    });
    async.series(calls, function (err) {
      if (err) common.flash(req, res, 'user setting async ' + err);
      result.sort(common.sortName);
      uncheck.sort(common.sortName);
      return common.render(req, res, 'user_setting', {title: '관리자 페이지', menu_admin: true, users: result, check: uncheck});
    });
  });
};

exports.signup = function (req, res, next) {
  if (req.method === 'GET') {
    return res.render('signup', {title: '회원가입'});
  }
  var username = req.body.username;
  User.findOne({'username': username}, function (err, user) {
    if (err) return next(err);
    if (user) {
      req.flash('error', common.ERR_USEDID);
      return res.redirect('/signup');
    }
    var password = req.body.password;
    if (password != req.body.pass2)
      return req.flash('error', common.ERR_PASS);
    var newUser = new User();
    newUser.username = username;
    newUser.local.password = newUser.generateHash(password);
    newUser.local.firstName = req.body.firstName;
    newUser.local.lastName = req.body.lastName;
    newUser.local.birth = new Date(req.body.birthYear, req.body.birthMonth - 1, req.body.birthDay);
    newUser.save();
    return res.redirect('/login');
  });
};

exports.support = function (req, res) {
  return common.support(req, res, Child, false);
};

exports.supporter = function (req, res) {
  common.isAdminCheck(req);
  if (typeof req.body.user == 'undefined')
    return res.send({err: common.N_INFO});
  User.findById(req.body.user, function (err, user) {
    if (err) return res.send({err: common.ERR});
    if (!user) return res.send({err: common.ERR_NO});
    if (user.isVisitor()) return res.send({err: common.ERR_SUP});
    if (!user.isSupporter()) {
      user.rank = user.rank + 1;
      user.save();
      return res.send({success: true});
    }
    return res.send({err: common.ERR});
  });
};

exports.supporter_del = function (req, res) {
  common.isAdminCheck(req);
  if (typeof req.body.user == 'undefined')
    return res.send({err: common.N_INFO});
  User.findById(req.body.user, function (err, user) {
    if (err) return res.send({err: common.ERR});
    if (!user) return res.send({err: common.ERR_NO});
    if (user.isVisitor()) return res.send({err: common.ERR_SUP});
    if (user.isSupporter()) {
      user.rank = 1;
      user.save();
      return res.send({success: true});
    }
    return res.send({err: common.ERR});
  });
};