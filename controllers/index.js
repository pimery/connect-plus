var common = require('../config/common');

exports.home = function (req, res) {
  return res.redirect('/home');
};

exports.info = function (req, res) {
  return common.render(req, res, 'info', {title: '소개', menu_info: true});
};

exports.signup = function (req, res) {
  res.render('signup', {title: '회원가입'});
};