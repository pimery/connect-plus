var Child = require('../models/child.js');
var User = require('../models/user.js');
var common = require('../config/common');
var async = require('async');
var fs = require('fs');
var multer = require('multer');

var calls = [];

function saveChild(child, req) {
  child.name = req.body.name;
  child.age = req.body.age;
  child.isMale = req.body.isMale;
  child.birth = new Date(req.body.birthYear, req.body.birthMonth - 1, req.body.birthDay);
  child.school = req.body.school;
  child.favoriteSubject = req.body.favoriteSubject;
  child.favoriteActivity = req.body.favoriteActivity;
  child.code = req.body.code;
  if (typeof req.file !== 'undefined') {
    if (child.photo != null && child.photo !== common.getFileExt(req.file.filename))
      common.unlink(child.getPhoto());
    child.photo = common.getFileExt(req.file.filename);
  }
  child.supports = [];
  if (Array.isArray(req.body.supports))
    for (var i = 0; i < req.body.supports.length; i++)
      common.push(child.supports, req.body.supports[i]);
  else if (req.body.supports) {
    common.push(child.supports, req.body.supports);
  } else child.supports = [];
  return child;
}

var child_id;

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, __dirname + '/../public' + common.CHILD_PATH);
  },
  filename: function (req, file, cb) {
    cb(null, child_id + '.' + common.getFileExt(file.originalname));
  }
});

exports.upload = multer({storage: storage});

exports.add = function (req, res) {
  common.isAdminCheck(req);
  if (req.method == 'GET') {
    child_id = new Child()._id;
    return common.render(req, res, 'child', {title: '아동 등록', menu_admin: true});
  }
  var newChild = new Child({_id: child_id});
  saveChild(newChild, req).save();
  async.filter([newChild._id], function (elem, callback) {
    Child.findOne({_id: elem}, function (err, doc) {
      callback(err == null && doc != null);
    });
  }, function () {
    return res.redirect('/child/' + newChild._id);
  });
};

exports.check = function (req, res) {
  common.isAdminCheck(req);
  if (typeof req.body.code === 'undefined')
    return res.send({mes: '고유번호를 확인해주세요'});
  Child.findOne({code: req.body.code}, function (err, child) {
    if (err) return res.send({mes: 'child add ' + err});
    if (child) return res.send({mes: '고유번호가 ' + child.name + ' 아동과 중복됩니다'});
    return res.send({success: true});
  });
};

exports.delete = function (req, res) {
  common.isAdminCheck(req);
  if (typeof req.body.target === 'undefined')
    return res.send({mes: common.N_INFO});
  Child.findById(req.body.target, function (err, child) {
    if (err) return res.send({mes: common.ERR});
    if (!child) return res.send({mes: common.ERR_CHILD});
    if (child.photo != null) common.unlink(child.getPhoto());
    child.remove();
    return res.send({success: true});
  });
};

exports.detail = function (req, res) {
  common.isSupportCheck(req);
  calls = [];
  var list = [];
  Child.findById(req.params.id, function (err, child) {
    if (err) return common.flash(req, res, 'child detail ' + err);
    if (!child) return common.flash(req, res, common.ERR_NO);
    if (child.supports.length) {
      calls.push(function (callback) {
        User.find({_id: {$in: child.supports}}, function (err, users) {
          if (err) callback(err);
          if (Array.isArray(users)) users.forEach(function (user) {
            list.push(user.getName());
          });
          callback(null);
        });
      });
    }
    async.series(calls, function (err) {
      if (err) return common.flash(req, res, 'child detail supports ' + err);
      return common.render(req, res, 'child_info', {
        title: child.name,
        menu_admin: true,
        child: child,
        supports: list
      });
    })
  });
};

exports.edit = function (req, res) {
  common.isAdminCheck(req);
  Child.findById(req.params.id, function (err, child) {
    if (err) return common.flash(req, res, 'child edit ' + err);
    if (!child) return common.flash(req, res, common.ERR_CHILD);
    if (req.method !== 'GET') {
      saveChild(child, req).save();
      return res.redirect('/child/' + child._id);
    }
    child_id = child._id;
    async.series(calls, function (err) {
      if (err) return common.flash(req, res, err);
      return common.render(req, res, 'child', {
        title: child.name,
        menu_admin: true,
        child: child
      });
    });
  });
};

exports.list = function (req, res) {
  return common.list(req, res, User, Child, 'name', true);
};

exports.setting = function (req, res) {
  common.isAdminCheck(req);
  if (req.method != 'GET')
    return common.render(req, res, 'child_setting');
  var result = [], calls = [];
  Child.find({}, null, {sort: 'name'}, function (err, children) {
    if (err) return common.flash(req, res, 'child setting ' + err);
    var list;
    if (Array.isArray(children)) children.forEach(function (child) {
      calls.push(function (callback) {
        User.find({_id: {$in: child.supports}}, function (err, users) {
          if (err) callback('child setting supports ' + err);
          list = [];
          if (Array.isArray(users)) users.forEach(function (user) {
            list.push(user.getName());
          });
          result.push({_id: child._id, name: child.name, supports: list});
          callback(null);
        })
      });
    });
    async.series(calls, function (err) {
      if (err) return common.flash(req, res, err);
      result.sort(common.sortName);
      return common.render(req, res, 'child_setting', {title: '관리자 페이지', menu_admin: true, children: result});
    });
  });
};

exports.support = function (req, res) {
  return common.support(req, res, User, true);
};