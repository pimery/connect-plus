var Article = require('../models/article.js');
var Child = require('../models/child');
var common = require('../config/common');
var async = require('async');
var multer = require('multer');
var fs = require('fs');

var calls = [];

function saveArticle(article, req) {
  var i;
  article.title = req.body.title;
  article.content = req.body.content;
  if (Array.isArray(req.body.remove)) req.body.remove.forEach(function (item) {
    if (!item) return;
    common.unlink(common.ART_PATH + item);
    article.photos.splice(article.photos.indexOf(item), 1);
  }); else if (req.body.remove) {
    common.unlink(common.ART_PATH + req.body.remove);
    article.photos.splice(article.photos.indexOf(req.body.remove), 1);
  }
  if (req.body.rmVideo) {
    common.unlink(article.getVideo());
    article.video = null;
  }
  if (typeof req.files !== 'undefined') {
    for (i = 0; i < req.files.length; i++) {
      if (req.files[i].fieldname == 'image') article.photos.push(req.files[i].filename);
      if (req.files[i].fieldname == 'video') article.video = req.files[i].filename;
    }
  }
  article.children = [];
  if (Array.isArray(req.body.children)) req.body.children.forEach(function (item) {
    if (!item) return;
    common.push(article.children, item);
  }); else if (req.body.children) article.children = req.body.children;
  article.editTime = Date.now();
  return article;
}

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    if (file.fieldname === 'video') cb(null, __dirname + '/../public' + common.VIDEO_PATH);
    else cb(null, __dirname + '/../public' + common.ART_PATH);
  },
  filename: function (req, file, cb) {
    cb(null, new Date().toString() + '_' + file.originalname);
  }
});

exports.upload = multer({
  storage: storage,
  limits: {fileSize: 10 * 1024 * 1024}
});

exports.all = function (req, res) {
  var list = [];
  Article.find({}, null, {sort: '-dateTime'}, function (err, articles) {
    if (err) return common.flash(req, res, 'article all err ' + err);
    if (Array.isArray(articles)) articles.forEach(function (item) {
      common.setTimeInfo(item);
      list.push(item);
    });
    return common.render(req, res, 'home', {title: '홈', menu_home: true, articles: list});
  });
};

exports.comm_del = function (req, res) {
  if (typeof req.body.article == 'undefined' || typeof req.body.user == 'undefined' || typeof req.body.comment == 'undefined')
    return res.send({err: common.N_INFO});
  common.isWriterCheck(req, req.body.user);
  Article.findById(req.body.article, function (err, article) {
    if (err) return res.send({err: common.ERR});
    if (!article) return res.send({err: common.ERR_NO});
    var index = -1;
    for (var i = 0; i < article.comments.length; i++) {
      if (article.comments[i]._id == req.body.comment) {
        index = i;
        break;
      }
    }
    article.comments.splice(index, 1);
    article.save();
    return res.send({success: true});
  })
};

exports.delete = function (req, res) {
  common.isAdminCheck(req);
  if (typeof req.body.target === 'undefined')
    return res.send({mes: common.N_INFO});
  Article.findById(req.body.target, function (err, article) {
    if (err) return res.send({mes: common.ERR});
    if (!article) return res.send({mes: common.ERR_NONE});
    article.photos.forEach(function (photo) {
      common.unlink(common.ART_PATH + photo);
    });
    common.unlink(article.getVideo());
    article.remove();
    return res.send({success: true});
  });
};

exports.detail = function (req, res) {
  return common.detail(req, res, Article, 'comments', true);
};

exports.edit = function (req, res) {
  common.isAdminCheck(req);
  calls = [];
  Article.findById(req.params.id, function (err, article) {
    if (err) return common.flash(req, res, 'article edit err ' + err);
    if (!article) return common.flash(req, res, common.ERR_NONE);
    if (req.method !== 'GET') {
      saveArticle(article, req).save();
      return res.redirect('/article/' + article._id);
    } else {
      var list = [];
      calls.push(function (callback) {
        Child.find({}, null, {sort: 'name'}, function (err, children) {
          if (err) callback('child err ' + err);
          if (Array.isArray(children)) children.forEach(function (child) {
            list.push({
              name: child.name + '(' + child.code + ')',
              _id: child._id,
              selected: common.isInList(article.children, child._id)
            });
          });
          callback(null);
        });
      });
      async.series(calls, function (err) {
        if (err) return common.flash(req, res, 'article edit async err ' + err);
        return common.render(req, res, 'article', {title: '글 수정', menu_home: true, article: article, children: list});
      });
    }
  });
};

exports.like = function (req, res) {
  common.isSupportCheck(req);
  common.react(req, res, Article, 'like', '/article/');
};

exports.reply = function (req, res) {
  return common.reply(req, res, Article, true);
};

exports.share = function (req, res) {
  common.isSupportCheck(req);
  common.react(req, res, Article, 'share', '/article/');
};

exports.write = function (req, res) {
  common.isAdminCheck(req);
  if (req.method === 'GET') {
    var list = [];
    Child.find({}, null, {sort: 'name'}, function (err, children) {
      if (err) return common.flash(req, res, 'article write err ' + err);
      if (Array.isArray(children)) children.forEach(function (child) {
        list.push({name: child.name + '(' + child.code + ')', _id: child._id});
      });
      return common.render(req, res, 'article', {title: '글쓰기', menu_home: true, children: list});
    });
  } else {
    var article = new Article();
    saveArticle(article, req).save();
    async.filter([article._id], function (elem, callback) {
      Article.findOne({_id: elem}, function (err, doc) {
        callback(err == null && doc != null);
      });
    }, function () {
      return res.redirect('/article/' + article._id);
    });
  }
};